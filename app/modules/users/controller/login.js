var app = angular.module('users')
app.controller('loginCtrl',loginCtrl);

loginCtrl.$inject = ['$scope', '$state','userServices'];
function loginCtrl($scope,$state,userServices){
    console.log('Login action is continued...');
   


    $scope.login=function(username,passw){
        
        userServices.loginUser(username,passw).then(function(result){
         
            if(result.data.user==username && !angular.isUndefined(username) && !angular.isUndefined(result.data.user)){
                console.log("success case");
                userServices.SetUserCookie(result.data.user,result.data.token);
                $state.go('admin.user.userAdd');      
            }else{
               $scope.errorMsg = "Invalid credentials"; 
               $state.go('login');     
            }  
        },function(err){
            $scope.errorMsg="Something went wrong...please try again!!!"; 
            $state.go('login'); 
        });            
    }
    
};