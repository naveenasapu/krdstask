var app = angular.module('users')
app.controller('UserAddCtrl',UserAddCtrl);

UserAddCtrl.$inject = ['$scope', '$state','userServices'];
function UserAddCtrl($scope,$state,userServices){
    userServices.getitemDoc().then(function(itemResult){
        console.log("item list.....",itemResult);
        $scope.itemList  = itemResult.data;
    })

    
    $scope.itemList = [];
    $scope.addItem = function(userData){
       
        userServices.addItem(userData).then(function(result){
            console.log("checkingg....",result);
            $scope.registerData.item = "";
            userServices.getitemDoc().then(function(itemResult){
                console.log("item list.....",itemResult);
                $scope.itemList  = itemResult.data;
            })
            if(result.data[0]){
                $scope.userError =  result.data;
            }else{
                $scope.userError = "added sucessfully";
            }
        },function(err){
            console.log("checkingg....err",err);        
        });
    }

    $scope.removeItem = function (item){
        userServices.removeItem(item._id).then(function(result){
            userServices.getitemDoc().then(function(itemResult){
                console.log("item list.....",itemResult);
                $scope.itemList  = itemResult.data;
            })
        },function(err){
            console.log("checkingg....err",err);        
        })
    }

    $scope.updateItem = function(item){
        userServices.updateItem(item).then(function(result){
            userServices.getitemDoc().then(function(itemResult){
                console.log("item list.....",itemResult);
                $scope.itemList  = itemResult.data;
            })
        },function(err){
            console.log("checkingg....err",err);        
        })
    }
    
    
}