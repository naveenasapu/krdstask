

var CONFIG = {};

CONFIG.PORT = (process.env.VCAP_APP_PORT || 3000);
CONFIG.DB_URL = 'mongodb://localhost:27017/angular5node';
CONFIG.SECRET_KEY = "nodeProjectSecret"


module.exports = CONFIG;