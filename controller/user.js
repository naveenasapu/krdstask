var bcrypt = require('bcrypt-nodejs');
var db = require('../controller/adaptor/mongodb.js');

module.exports = function(){

    var router = {};

    router.getUserList = function(req,res){        
        db.GetDocument("users",{},{},{},function(err,result){

            if(err){
                res.send(err);
            }

            res.send(result);
        });
    }

    router.addItem = function(req,res){
        
        req.checkBody('item', 'fill email').notEmpty();
        
        var errors = req.validationErrors();
        if (errors) {
            res.send(errors);
            return;
        }
       console.log("sssssss",req.body);
        
        db.InsertDocument("item",req.body,function(err,result){
            console.log("checking...",err,result);
            if(err){
                res.send(err);
            }

            res.send(result);
        })

        
    }

    router.getitemDoc = function(req,res){
        db.GetDocument("item",{ },{},{},function(err,result){
            console.log("checking...",err,result);
            if(err){
                res.send(err);
            }

            res.send(result);
        });
    }
    router.removeitemDoc = function(req,res){
        db.DeleteDocument('item',{ '_id':req.body.item_id},function(err,result){
            console.log("checking...",err,result);
            if(err){
                res.send(err);
            }

            res.send(result);
        })
    }

    router.updateItem = function(req,res){
        console.log("update item...*****",req.body);
        db.UpdateDocument("item",{ "username":req.body.id },req.body,{multi: true },function(err,result){
            console.log("checking...",err,result);
            if(err){
                res.send(err);
            }

            res.send(result);
        })


    }



    return router;
}