var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ITEM_SCHEMA = {};
ITEM_SCHEMA.ITEM = {
    item: { type: String, lowercase: true, index: { unique: true }, trim: true },
    selected:{ type: Boolean }
}

module.exports = ITEM_SCHEMA;